# Go Fiber Clean Architecture

This project was created to learn golang with go fiber framework

## How To Run

1. Run application with command `go run main.go`

## Feature

- [x] Database ORM
- [x] Database Relational
- [x] Json Validation
- [x] JWT Security
- [x] Open API / Swagger
- [x] Http Client
- [x] Error Handling
- [x] Logging
- [x] Cache
- [x] Consul
- [x] Xtra (Exmple Database & Json)