package repository

import (
	"context"

	"github.com/go-fiber-clean-architecture/model/entity"
)

type AuthRepository interface {
	Authentication(ctx context.Context, username string) (entity.UserEntity, error)
}
