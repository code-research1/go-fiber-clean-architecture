package entity

import (
	"time"
)

type RoleEntity struct {
	IdMasterRoles string    `gorm:"primaryKey;column:id_master_roles"`
	RoleName      string    `gorm:"role_name"`
	Description   string    `gorm:"description"`
	CreatedBy     string    `gorm:"created_by"`
	UpdatedBy     string    `gorm:"updated_by"`
	IsActive      string    `gorm:"is_active"`
	CreatedAt     time.Time `gorm:"created_at"`
	UpdatedAt     time.Time `gorm:"updated_at"`
}

func (RoleEntity) TableName() string {
	return "db_staterkit.master_roles"
}
