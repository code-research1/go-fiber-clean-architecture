package entity

import (
	"time"
)

type UserEntity struct {
	IdMasterUsers   string     `gorm:"column:id_master_users"`
	IdMasterRoles   string     `gorm:"column:id_master_roles"`
	Fullname        string     `gorm:"column:fullname"`
	Username        string     `gorm:"column:username"`
	IsGender        string     `gorm:"column:is_gender"`
	Address         string     `gorm:"column:address"`
	HpNumber        string     `gorm:"column:hp_number"`
	DateActivation  time.Time  `gorm:"date_activation"`
	Email           string     `gorm:"email"`
	EmailVerifiedAt time.Time  `gorm:"email_verified_at"`
	Password        string     `gorm:"password"`
	UrlPhoto        string     `gorm:"url_photo"`
	CreatedBy       string     `gorm:"created_by"`
	UpdatedBy       string     `gorm:"updated_y"`
	IsActive        string     `gorm:"is_active"`
	CreatedAt       time.Time  `gorm:"created_at"`
	UpdatedAt       time.Time  `gorm:"updated_at"`
	Role            RoleEntity `gorm:"foreignKey:IdMasterRoles;references:IdMasterRoles"`
}

func (UserEntity) TableName() string {
	return "db_staterkit.master_users"
}
