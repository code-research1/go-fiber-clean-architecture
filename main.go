package main

import (
	_ "github.com/go-fiber-clean-architecture/docs"
	"github.com/go-fiber-clean-architecture/exception"
	"github.com/go-fiber-clean-architecture/utils"

	controller "github.com/go-fiber-clean-architecture/api/controllers"
	implRepository "github.com/go-fiber-clean-architecture/repository/impl"
	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/cors"
	"github.com/gofiber/fiber/v2/middleware/recover"
	"github.com/gofiber/swagger"
)

// @title Go Fiber Clean Architecture
// @version 1.0.0
// @description Baseline project using Go Fiber
// @termsOfService http://swagger.io/terms/
// @contact.name API Support
// @contact.email fiber@swagger.io
// @license.name Apache 2.0
// @license.url http://www.apache.org/licenses/LICENSE-2.0.html
// @host localhost:5000
// @BasePath /
// @schemes http https
// @securityDefinitions.apikey JWT
// @in header
// @name Authorization
// @description Authorization For JWT
func main() {

	//load consul config
	loadConsulConfig, _ := utils.LoadConsulConfig()
	// Impor konfigurasi ke Consul
	_ = utils.ImportConfigToConsul(loadConsulConfig)

	//setup configuration
	config := utils.NewEnv(loadConsulConfig)
	database := utils.NewDatabase(config)
	// redis := configuration.NewRedis(config)

	//repository
	authRepository := implRepository.NewAuthRepositoryImpl(database)
	roleRepository := implRepository.NewRoleRepositoryImpl(database)
	userRepository := implRepository.NewUserRepositoryImpl(database)

	//controller
	authController := controller.NewAuthController(&authRepository, config)
	roleController := controller.NewRoleController(&roleRepository, config)
	userController := controller.NewUserController(&userRepository, config)
	commonController := controller.NewCommonController(config)
	httpBinController := controller.NewHttpBinController()

	//setup fiber
	app := fiber.New(utils.NewFiberConfiguration())
	app.Use(recover.New())
	app.Use(cors.New())

	//routing
	authController.Route(app)
	roleController.Route(app)
	userController.Route(app)
	commonController.Route(app)
	httpBinController.Route(app)

	//swagger
	app.Get("/swagger/*", swagger.HandlerDefault)

	//start app
	// fmt.Println("11================= ::" + config.ServerPort)
	err := app.Listen(config.ServerPort)
	exception.PanicLogging(err)
}
