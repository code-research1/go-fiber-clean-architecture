package impl

import (
	"context"
	"errors"

	"github.com/go-fiber-clean-architecture/model/entity"
	"github.com/go-fiber-clean-architecture/repository"
	"gorm.io/gorm"
)

func NewAuthRepositoryImpl(DB *gorm.DB) repository.AuthRepository {
	return &authRepositoryImpl{DB: DB}
}

type authRepositoryImpl struct {
	*gorm.DB
}

func (repository *authRepositoryImpl) Authentication(ctx context.Context, username string) (entity.UserEntity, error) {
	var userResult entity.UserEntity
	result := repository.DB.WithContext(ctx).
		Where("master_users.username = ? and master_users.is_active = ?", username, "ACTIVED").
		Find(&userResult)
	if result.RowsAffected == 0 {
		return entity.UserEntity{}, errors.New("user not found")
	}
	return userResult, nil
}
