package impl

import (
	"context"
	"errors"
	"strings"

	"github.com/go-fiber-clean-architecture/exception"
	"github.com/go-fiber-clean-architecture/model/criteria"
	"github.com/go-fiber-clean-architecture/model/entity"
	"github.com/go-fiber-clean-architecture/repository"
	"github.com/go-fiber-clean-architecture/utils/paginate"
	"github.com/google/uuid"
	"gorm.io/gorm"
)

func NewUserRepositoryImpl(DB *gorm.DB) repository.UserRepository {
	return &userRepositoryImpl{DB: DB}
}

type userRepositoryImpl struct {
	*gorm.DB
}

func (repository *userRepositoryImpl) Insert(ctx context.Context, user entity.UserEntity) entity.UserEntity {
	user.IdMasterUsers = uuid.New().String()
	err := repository.DB.WithContext(ctx).Create(&user).Error
	exception.PanicLogging(err)
	return user
}

func (repository *userRepositoryImpl) Update(ctx context.Context, user entity.UserEntity) entity.UserEntity {
	err := repository.DB.WithContext(ctx).Where("id_master_users = ?", user.IdMasterUsers).Updates(&user).Error
	exception.PanicLogging(err)
	return user
}

func (repository *userRepositoryImpl) FindByParam(ctx context.Context, key string, value string) (entity.UserEntity, error) {
	var user entity.UserEntity
	result := repository.DB.WithContext(ctx).Preload("Role").Unscoped().Where(key, value).Find(&user)
	if result.RowsAffected == 0 {
		return entity.UserEntity{}, errors.New("user Not Found")
	}
	return user, nil
}

func (repository *userRepositoryImpl) FindById(ctx context.Context, id string) (entity.UserEntity, error) {
	var user entity.UserEntity
	result := repository.DB.WithContext(ctx).Preload("Role").Unscoped().Where("id_master_users = ?", id).First(&user)
	if result.RowsAffected == 0 {
		return entity.UserEntity{}, errors.New("user Not Found")
	}
	return user, nil
}

func (repository *userRepositoryImpl) FindAll(ctx context.Context, paging paginate.Datapaging, options criteria.GetListOfUserOptions) (int64, *[]entity.UserEntity, error) {
	var userDatas []entity.UserEntity

	query := repository.DB.WithContext(ctx).Table("db_staterkit.master_users").Preload("Role")

	if len(options.Status) > 0 {
		statusValuesStr, err := paginate.PrepareStatusValues(options.Status)
		if err != nil {
			return 0, nil, err
		}

		query.Where("is_active IN ? ", statusValuesStr)
	}

	if len(paging.FilterValue) > 0 && len(options.SearchBy) > 0 {
		if options.SearchBy == "fullname" {
			query.Where("upper(fullname) like  ? ", "%"+strings.ToUpper(paging.FilterValue)+"%")
		}

		if options.SearchBy == "username" {
			query.Where("upper(username) like  ? ", "%"+strings.ToUpper(paging.FilterValue)+"%")
		}

		if options.SearchBy == "address" {
			query.Where("upper(address) like  ? ", "%"+strings.ToUpper(paging.FilterValue)+"%")
		}
	}

	var totalCount int64
	query.Model(&userDatas).Count(&totalCount)
	if query.Error != nil {
		return 0, nil, query.Error
	}

	paging.BuildQueryGORM(query).Find(&userDatas)

	return totalCount, &userDatas, query.Error
}
