package controller

import (
	"net/http"
	"strings"

	middleware "github.com/go-fiber-clean-architecture/api/middlewares"
	"github.com/go-fiber-clean-architecture/constant"
	"github.com/go-fiber-clean-architecture/exception"
	"github.com/go-fiber-clean-architecture/model/criteria"
	"github.com/go-fiber-clean-architecture/model/entity"
	"github.com/go-fiber-clean-architecture/model/result"
	"github.com/go-fiber-clean-architecture/repository"
	"github.com/go-fiber-clean-architecture/utils"
	"github.com/go-fiber-clean-architecture/utils/paginate"
	"github.com/go-fiber-clean-architecture/validation"
	"github.com/gofiber/fiber/v2"
	"github.com/google/uuid"
)

type UserController struct {
	repository.UserRepository
	utils.Config
}

func NewUserController(userRepository *repository.UserRepository, config utils.Config) *UserController {
	return &UserController{UserRepository: *userRepository, Config: config}
}

func (controller UserController) Route(app *fiber.App) {

	group := app.Group(controller.Config.Route)

	group.Post("/user/store", middleware.AuthenticationJWT(controller.Config), controller.Create)
	group.Put("/user/update/:idMasterUsers", middleware.AuthenticationJWT(controller.Config), controller.Update)
	group.Put("/user/updateIsActive/:idMasterUsers", middleware.AuthenticationJWT(controller.Config), controller.UpdateIsActive)
	group.Post("/user/getAllDataUserByParam", middleware.AuthenticationJWT(controller.Config), controller.GetAllDataUserByParam)
	group.Get("/user/getDataById/:idMasterUsers", middleware.AuthenticationJWT(controller.Config), controller.FindById)
	group.Get("/user", middleware.AuthenticationJWT(controller.Config), controller.FindAll)
}

// Create func create user.
// @Description create user.
// @Summary create user
// @Tags User
// @Accept json
// @Produce json
// @Param request body criteria.StoreUserCriteria true "Request Body"
// @Success 200 {object} result.ResponseResult
// @Security JWT
// @Router /user/create [post]
func (controller UserController) Create(c *fiber.Ctx) error {
	var requestModel criteria.StoreUserCriteria

	err := c.BodyParser(&requestModel)
	exception.PanicLogging(err)
	if err != nil {
		utils.NewLogger().Info(constant.ERR_OBJECT_VALIDATION_DETAIL, ": ", err.Error())
	}

	validation.ValidateCriteria(requestModel)

	genPass, _ := utils.GeneratePassword()

	paramStore := entity.UserEntity{
		IdMasterUsers: uuid.New().String(),
		IdMasterRoles: requestModel.IdMasterRoles,
		Fullname:      requestModel.Fullname,
		Username:      requestModel.Username,
		IsGender:      requestModel.IsGender,
		Address:       requestModel.Address,
		HpNumber:      requestModel.HpNumber,
		Email:         requestModel.Email,
		Password:      genPass,
		CreatedBy:     requestModel.CreatedBy,
		IsActive:      constant.ACTIVED,
	}

	_ = controller.UserRepository.Insert(c.Context(), paramStore)

	return c.JSON(result.ResponseResult{
		ResponseCode:        http.StatusOK,
		ResponseDescription: constant.SUCCESS,
		ResponseTime:        utils.DateToStdNow(),
		ResponseDatas:       constant.SUCCESSFULLY_ADD,
	})
}

// Update func update one exists user.
// @Description update one exists user.
// @Summary update one exists user
// @Tags User
// @Accept json
// @Produce json
// @Param request body criteria.UpdateUserCriteria true "Request Body"
// @Success 200 {object} result.ResponseResult
// @Security JWT
// @Router /user/update/{idMasterUsers} [put]
func (controller UserController) Update(c *fiber.Ctx) error {
	var requestModel criteria.UpdateUserCriteria

	err := c.BodyParser(&requestModel)
	exception.PanicLogging(err)
	if err != nil {
		utils.NewLogger().Info(constant.ERR_OBJECT_VALIDATION_DETAIL, ": ", err.Error())
	}

	idStr := c.Params("idMasterUsers")
	validation.ValidateCriteria(requestModel)

	paramStore := entity.UserEntity{
		IdMasterUsers: idStr,
		IdMasterRoles: requestModel.IdMasterRoles,
		Fullname:      requestModel.Fullname,
		Username:      requestModel.Username,
		IsGender:      requestModel.IsGender,
		Address:       requestModel.Address,
		HpNumber:      requestModel.HpNumber,
		Email:         requestModel.Email,
		UpdatedBy:     requestModel.UpdatedBy,
	}
	_, err = controller.UserRepository.FindById(c.Context(), paramStore.IdMasterUsers)
	if err != nil {
		utils.NewLogger().Info(constant.DATA_NOT_FOUND, ": ", err.Error())
		panic(exception.NotFoundError{
			Message: err.Error(),
		})
	}

	_ = controller.UserRepository.Update(c.Context(), paramStore)
	return c.JSON(result.ResponseResult{
		ResponseCode:        http.StatusOK,
		ResponseDescription: constant.SUCCESS,
		ResponseTime:        utils.DateToStdNow(),
		ResponseDatas:       constant.SUCCESSFULLY_UPDATE,
	})
}

// Update func update isactive one exists user.
// @Description update isactive one exists user.
// @Summary update isactive one exists user
// @Tags User
// @Accept json
// @Produce json
// @Param request body criteria.UpdateIsActiveUserCriteria true "Request Body"
// @Success 200 {object} result.ResponseResult
// @Security JWT
// @Router /user/updateIsActive/{idMasterUsers} [put]
func (controller UserController) UpdateIsActive(c *fiber.Ctx) error {
	var requestModel criteria.UpdateIsActiveUserCriteria

	err := c.BodyParser(&requestModel)
	exception.PanicLogging(err)
	if err != nil {
		utils.NewLogger().Info(constant.ERR_OBJECT_VALIDATION_DETAIL, ": ", err.Error())
	}

	idStr := c.Params("idMasterUsers")
	validation.ValidateCriteria(requestModel)

	paramStore := entity.UserEntity{
		IdMasterUsers: idStr,
		IsActive:      requestModel.IsActive,
		UpdatedBy:     requestModel.UpdatedBy,
	}

	_, err = controller.UserRepository.FindById(c.Context(), paramStore.IdMasterUsers)
	if err != nil {
		utils.NewLogger().Info(constant.DATA_NOT_FOUND, ": ", err.Error())
		panic(exception.NotFoundError{
			Message: err.Error(),
		})
	}

	_ = controller.UserRepository.Update(c.Context(), paramStore)
	return c.JSON(result.ResponseResult{
		ResponseCode:        http.StatusOK,
		ResponseDescription: constant.SUCCESS,
		ResponseTime:        utils.DateToStdNow(),
		ResponseDatas:       constant.SUCCESSFULLY_DELETE,
	})
}

// GetAllDataUserByParam func gets all exists user by param.
// @Description Get all exists user by param.
// @Summary get all exists user by param
// @Tags User
// @Accept json
// @Produce json
// @Param request body criteria.UserSearchCriteria true "Request Body"
// @Success 200 {object} result.ResponseResult
// @Security JWT
// @Router /user/getAllDataUserByParam [post]
func (controller UserController) GetAllDataUserByParam(c *fiber.Ctx) error {
	var requestModel criteria.UserSearchCriteria

	err := c.BodyParser(&requestModel)
	exception.PanicLogging(err)
	if err != nil {
		utils.NewLogger().Info(constant.ERR_OBJECT_VALIDATION_DETAIL, ": ", err.Error())
	}

	validation.ValidateCriteria(requestModel)

	resultData, err := controller.UserRepository.FindByParam(c.Context(), requestModel.Key, requestModel.Value)
	if err != nil {
		utils.NewLogger().Info(constant.DATA_NOT_FOUND, ": ", err.Error())
		panic(exception.NotFoundError{
			Message: err.Error(),
		})
	}

	return c.JSON(result.ResponseResult{
		ResponseCode:        http.StatusOK,
		ResponseDescription: constant.SUCCESS,
		ResponseTime:        utils.DateToStdNow(),
		ResponseDatas:       resultData,
	})
}

// FindById func gets one exists user.
// @Description Get one exists user.
// @Summary get one exists user
// @Tags User
// @Accept json
// @Produce json
// @Param id path string true "user Id"
// @Success 200 {object} result.ResponseResult
// @Security JWT
// @Router /user/getDataById/{idMasterUsers} [get]
func (controller UserController) FindById(c *fiber.Ctx) error {
	id := c.Params("idMasterUsers")

	resultData, err := controller.UserRepository.FindById(c.Context(), id)
	if err != nil {
		utils.NewLogger().Info(constant.DATA_NOT_FOUND, ": ", err.Error())
		panic(exception.NotFoundError{
			Message: err.Error(),
		})
	}

	return c.JSON(result.ResponseResult{
		ResponseCode:        http.StatusOK,
		ResponseDescription: constant.SUCCESS,
		ResponseTime:        utils.DateToStdNow(),
		ResponseDatas:       resultData,
	})
}

// FindAll func gets all exists users.
// @Description Get all exists users.
// @Summary get all exists users
// @Tags User
// @Accept json
// @Produce json
// @Success 200 {object} result.ResponseResult
// @Security JWT
// @Router /user [get]
func (controller UserController) FindAll(c *fiber.Ctx) error {

	paging := paginate.PreparePagination(map[string]string{
		"search":         c.Query("search"),
		"sort_by":        c.Query("sort_by"),
		"sort_direction": c.Query("sort_direction"),
		"limit":          c.Query("limit"),
		"page":           c.Query("page"),
	}, []string{
		"id",
		"created_at",
		"updated_at",
	})

	searchByQuery := c.Query("search_by")
	statusByQUery := c.Query("status")
	status := []string{}
	if statusByQUery != "" {
		status = strings.Split(statusByQUery, ",")
	}

	options := criteria.GetListOfUserOptions{
		SearchBy: searchByQuery,
		Status:   status,
	}

	totalCount, resultData, err := controller.UserRepository.FindAll(c.Context(), paging, options)

	if err != nil {
		utils.NewLogger().Info(constant.DATA_NOT_FOUND, ": ", err.Error())
		panic(exception.NotFoundError{
			Message: err.Error(),
		})
	}

	data := result.DataPagingResult{
		PageNumber:       paging.Page,
		PageSize:         paging.Limit,
		TotalRecordCount: totalCount,
		Records:          resultData,
	}

	return c.JSON(result.ResponseResult{
		ResponseCode:        http.StatusOK,
		ResponseDescription: constant.SUCCESS,
		ResponseTime:        utils.DateToStdNow(),
		ResponseDatas:       data,
	})
}
