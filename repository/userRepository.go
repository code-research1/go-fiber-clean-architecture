package repository

import (
	"context"

	"github.com/go-fiber-clean-architecture/model/criteria"
	"github.com/go-fiber-clean-architecture/model/entity"
	"github.com/go-fiber-clean-architecture/utils/paginate"
)

type UserRepository interface {
	Insert(ctx context.Context, user entity.UserEntity) entity.UserEntity
	Update(ctx context.Context, user entity.UserEntity) entity.UserEntity
	FindByParam(ctx context.Context, key string, value string) (entity.UserEntity, error)
	FindById(ctx context.Context, id string) (entity.UserEntity, error)
	FindAll(ctx context.Context, paging paginate.Datapaging, options criteria.GetListOfUserOptions) (int64, *[]entity.UserEntity, error)
}
