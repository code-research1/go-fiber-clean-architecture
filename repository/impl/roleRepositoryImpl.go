package impl

import (
	"context"
	"errors"
	"strings"

	"github.com/go-fiber-clean-architecture/exception"
	"github.com/go-fiber-clean-architecture/model/criteria"
	"github.com/go-fiber-clean-architecture/model/entity"
	"github.com/go-fiber-clean-architecture/repository"
	"github.com/go-fiber-clean-architecture/utils/paginate"
	"gorm.io/gorm"
)

func NewRoleRepositoryImpl(DB *gorm.DB) repository.RoleRepository {
	return &roleRepositoryImpl{DB: DB}
}

type roleRepositoryImpl struct {
	*gorm.DB
}

func (repository *roleRepositoryImpl) Insert(ctx context.Context, role entity.RoleEntity) entity.RoleEntity {
	err := repository.DB.WithContext(ctx).Create(&role).Error
	exception.PanicLogging(err)
	return role
}

func (repository *roleRepositoryImpl) Update(ctx context.Context, role entity.RoleEntity) entity.RoleEntity {
	err := repository.DB.WithContext(ctx).Where("id_master_roles = ?", role.IdMasterRoles).Updates(&role).Error
	exception.PanicLogging(err)
	return role
}

func (repository *roleRepositoryImpl) FindById(ctx context.Context, id string) (entity.RoleEntity, error) {
	var role entity.RoleEntity
	result := repository.DB.WithContext(ctx).Unscoped().Where("id_master_roles = ?", id).First(&role)
	if result.RowsAffected == 0 {
		return entity.RoleEntity{}, errors.New("role Not Found")
	}
	return role, nil
}

func (repository *roleRepositoryImpl) FindAll(ctx context.Context, paging paginate.Datapaging, options criteria.GetListOfRoleOptions) (int64, *[]entity.RoleEntity, error) {
	// var roles []entity.RoleEntity
	// repository.DB.WithContext(ctx).Find(&roles)
	// return roles

	var roleDatas []entity.RoleEntity
	query := repository.DB.WithContext(ctx).Table("db_staterkit.master_roles")

	if len(options.Status) > 0 {
		statusValuesStr, err := paginate.PrepareStatusValues(options.Status)
		if err != nil {
			return 0, nil, err
		}

		query.Where("is_active IN ? ", statusValuesStr)
	}

	if len(paging.FilterValue) > 0 && len(options.SearchBy) > 0 {
		if options.SearchBy == "role_name" {
			query.Where("upper(role_name) like  ? ", "%"+strings.ToUpper(paging.FilterValue)+"%")
		}
	}

	var totalCount int64
	query.Model(&roleDatas).Count(&totalCount)
	if query.Error != nil {
		return 0, nil, query.Error
	}

	paging.BuildQueryGORM(query).Find(&roleDatas)

	return totalCount, &roleDatas, query.Error
}
