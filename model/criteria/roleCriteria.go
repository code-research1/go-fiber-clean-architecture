package criteria

type StoreRoleCriteria struct {
	RoleName    string `json:"roleName" validate:"required"`
	Description string `json:"description" validate:"required"`
	CreatedBy   string `json:"createdBy" validate:"required"`
}

type UpdateRoleCriteria struct {
	RoleName    string `json:"roleName" validate:"required"`
	Description string `json:"description" validate:"required"`
	UpdatedBy   string `json:"updatedBy" validate:"required"`
}

type UpdateIsActiveRoleCriteria struct {
	IsActive  string `json:"isActive" validate:"required"`
	UpdatedBy string `json:"updatedBy" validate:"required"`
}

type GetListOfRoleOptions struct {
	SearchBy string   `json:"search_by"`
	Status   []string `json:"status"`
}
