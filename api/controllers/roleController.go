package controller

import (
	"net/http"
	"strings"

	middleware "github.com/go-fiber-clean-architecture/api/middlewares"
	"github.com/go-fiber-clean-architecture/constant"
	"github.com/go-fiber-clean-architecture/exception"
	"github.com/go-fiber-clean-architecture/model/criteria"
	"github.com/go-fiber-clean-architecture/model/entity"
	"github.com/go-fiber-clean-architecture/model/result"
	"github.com/go-fiber-clean-architecture/repository"
	"github.com/go-fiber-clean-architecture/utils"
	"github.com/go-fiber-clean-architecture/utils/paginate"
	"github.com/go-fiber-clean-architecture/validation"
	"github.com/gofiber/fiber/v2"
	"github.com/google/uuid"
)

type RoleController struct {
	repository.RoleRepository
	utils.Config
}

func NewRoleController(roleRepository *repository.RoleRepository, config utils.Config) *RoleController {
	return &RoleController{RoleRepository: *roleRepository, Config: config}
}

func (controller RoleController) Route(app *fiber.App) {

	group := app.Group(controller.Config.Route)

	group.Post("/role/store", middleware.AuthenticationJWT(controller.Config), controller.Create)
	group.Put("/role/update/:idMasterRoles", middleware.AuthenticationJWT(controller.Config), controller.Update)
	group.Put("/role/updateIsActive/:idMasterRoles", middleware.AuthenticationJWT(controller.Config), controller.UpdateIsActive)
	group.Get("/role/getDataById/:idMasterRoles", middleware.AuthenticationJWT(controller.Config), controller.FindById)
	group.Get("/role", middleware.AuthenticationJWT(controller.Config), controller.FindAll)
}

// Create func create role.
// @Description create role.
// @Summary create role
// @Tags Role
// @Accept json
// @Produce json
// @Param request body criteria.StoreRoleCriteria true "Request Body"
// @Success 200 {object} result.ResponseResult
// @Security JWT
// @Router /role/create [post]
func (controller RoleController) Create(c *fiber.Ctx) error {
	var requestModel criteria.StoreRoleCriteria

	err := c.BodyParser(&requestModel)
	exception.PanicLogging(err)

	validation.ValidateCriteria(requestModel)

	paramStore := entity.RoleEntity{
		IdMasterRoles: uuid.New().String(),
		RoleName:      requestModel.RoleName,
		Description:   requestModel.Description,
		CreatedBy:     requestModel.CreatedBy,
		IsActive:      constant.ACTIVED,
	}

	_ = controller.RoleRepository.Insert(c.Context(), paramStore)

	return c.JSON(result.ResponseResult{
		ResponseCode:        http.StatusOK,
		ResponseDescription: constant.SUCCESS,
		ResponseTime:        utils.DateToStdNow(),
		ResponseDatas:       constant.SUCCESSFULLY_ADD,
	})
}

// Update func update one exists role.
// @Description update one exists role.
// @Summary update one exists role
// @Tags Role
// @Accept json
// @Produce json
// @Param request body criteria.UpdateRoleCriteria true "Request Body"
// @Success 200 {object} result.ResponseResult
// @Security JWT
// @Router /role/update/{idMasterRoles} [put]
func (controller RoleController) Update(c *fiber.Ctx) error {
	var requestModel criteria.UpdateRoleCriteria

	err := c.BodyParser(&requestModel)
	exception.PanicLogging(err)
	if err != nil {
		utils.NewLogger().Info(constant.ERR_OBJECT_VALIDATION_DETAIL, ": ", err.Error())
	}

	idStr := c.Params("idMasterRoles")
	validation.ValidateCriteria(requestModel)

	paramStore := entity.RoleEntity{
		IdMasterRoles: idStr,
		RoleName:      requestModel.RoleName,
		Description:   requestModel.Description,
		UpdatedBy:     requestModel.UpdatedBy,
	}

	_, err = controller.RoleRepository.FindById(c.Context(), paramStore.IdMasterRoles)
	if err != nil {
		utils.NewLogger().Info(constant.DATA_NOT_FOUND, err.Error())
		panic(exception.NotFoundError{
			Message: err.Error(),
		})
	}

	_ = controller.RoleRepository.Update(c.Context(), paramStore)
	return c.JSON(result.ResponseResult{
		ResponseCode:        http.StatusOK,
		ResponseDescription: constant.SUCCESS,
		ResponseTime:        utils.DateToStdNow(),
		ResponseDatas:       constant.SUCCESSFULLY_UPDATE,
	})
}

// Update func update isactive one exists role.
// @Description update isactive one exists role.
// @Summary update isactive one exists role
// @Tags Role
// @Accept json
// @Produce json
// @Param request body criteria.UpdateIsActiveRoleCriteria true "Request Body"
// @Success 200 {object} result.ResponseResult
// @Security JWT
// @Router /role/updateIsActive/{idMasterRoles} [put]
func (controller RoleController) UpdateIsActive(c *fiber.Ctx) error {
	var requestModel criteria.UpdateIsActiveRoleCriteria

	err := c.BodyParser(&requestModel)
	exception.PanicLogging(err)
	if err != nil {
		utils.NewLogger().Info(constant.ERR_OBJECT_VALIDATION_DETAIL, ": ", err.Error())
	}

	idStr := c.Params("idMasterRoles")
	validation.ValidateCriteria(requestModel)

	paramStore := entity.RoleEntity{
		IdMasterRoles: idStr,
		IsActive:      requestModel.IsActive,
		UpdatedBy:     requestModel.UpdatedBy,
	}

	_, err = controller.RoleRepository.FindById(c.Context(), paramStore.IdMasterRoles)
	if err != nil {
		utils.NewLogger().Info(constant.DATA_NOT_FOUND, ": ", err.Error())
		panic(exception.NotFoundError{
			Message: err.Error(),
		})
	}

	_ = controller.RoleRepository.Update(c.Context(), paramStore)
	return c.JSON(result.ResponseResult{
		ResponseCode:        http.StatusOK,
		ResponseDescription: constant.SUCCESS,
		ResponseTime:        utils.DateToStdNow(),
		ResponseDatas:       constant.SUCCESSFULLY_DELETE,
	})
}

// FindById func gets one exists role.
// @Description Get one exists role.
// @Summary get one exists role
// @Tags Role
// @Accept json
// @Produce json
// @Param id path string true "role Id"
// @Success 200 {object} result.ResponseResult
// @Security JWT
// @Router /role/getDataById/{idMasterRoles} [get]
func (controller RoleController) FindById(c *fiber.Ctx) error {
	id := c.Params("idMasterRoles")

	resultData, err := controller.RoleRepository.FindById(c.Context(), id)
	if err != nil {
		utils.NewLogger().Info(constant.DATA_NOT_FOUND, ": ", err.Error())
		panic(exception.NotFoundError{
			Message: err.Error(),
		})
	}

	return c.JSON(result.ResponseResult{
		ResponseCode:        http.StatusOK,
		ResponseDescription: constant.SUCCESS,
		ResponseTime:        utils.DateToStdNow(),
		ResponseDatas:       resultData,
	})
}

// FindAll func gets all exists roles.
// @Description Get all exists roles.
// @Summary get all exists roles
// @Tags Role
// @Accept json
// @Produce json
// @Success 200 {object} result.ResponseResult
// @Security JWT
// @Router /role [get]
func (controller RoleController) FindAll(c *fiber.Ctx) error {

	paging := paginate.PreparePagination(map[string]string{
		"search":         c.Query("search"),
		"sort_by":        c.Query("sort_by"),
		"sort_direction": c.Query("sort_direction"),
		"limit":          c.Query("limit"),
		"page":           c.Query("page"),
	}, []string{
		"id",
		"created_at",
		"updated_at",
	})

	searchByQuery := c.Query("search_by")
	statusByQUery := c.Query("status")
	status := []string{}
	if statusByQUery != "" {
		status = strings.Split(statusByQUery, ",")
	}

	options := criteria.GetListOfRoleOptions{
		SearchBy: searchByQuery,
		Status:   status,
	}

	totalCount, resultData, err := controller.RoleRepository.FindAll(c.Context(), paging, options)

	if err != nil {
		utils.NewLogger().Info(constant.DATA_NOT_FOUND, ": ", err.Error())
		panic(exception.NotFoundError{
			Message: err.Error(),
		})
	}

	data := result.DataPagingResult{
		PageNumber:       paging.Page,
		PageSize:         paging.Limit,
		TotalRecordCount: totalCount,
		Records:          resultData,
	}

	return c.JSON(result.ResponseResult{
		ResponseCode:        http.StatusOK,
		ResponseDescription: constant.SUCCESS,
		ResponseTime:        utils.DateToStdNow(),
		ResponseDatas:       data,
	})
}
