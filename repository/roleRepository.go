package repository

import (
	"context"

	"github.com/go-fiber-clean-architecture/model/criteria"
	"github.com/go-fiber-clean-architecture/model/entity"
	"github.com/go-fiber-clean-architecture/utils/paginate"
)

type RoleRepository interface {
	Insert(ctx context.Context, role entity.RoleEntity) entity.RoleEntity
	Update(ctx context.Context, role entity.RoleEntity) entity.RoleEntity
	FindById(ctx context.Context, id string) (entity.RoleEntity, error)
	FindAll(ctx context.Context, paging paginate.Datapaging, options criteria.GetListOfRoleOptions) (int64, *[]entity.RoleEntity, error)
}
