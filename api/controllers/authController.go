package controller

import (
	"net/http"

	"github.com/go-fiber-clean-architecture/constant"
	"github.com/go-fiber-clean-architecture/exception"
	"github.com/go-fiber-clean-architecture/model/criteria"
	"github.com/go-fiber-clean-architecture/model/result"
	"github.com/go-fiber-clean-architecture/repository"
	"github.com/go-fiber-clean-architecture/utils"
	"github.com/gofiber/fiber/v2"
	"golang.org/x/crypto/bcrypt"
)

func NewAuthController(authRepository *repository.AuthRepository, config utils.Config) *AuthController {
	return &AuthController{AuthRepository: *authRepository, Config: config}
}

type AuthController struct {
	repository.AuthRepository
	utils.Config
}

func (controller AuthController) Route(app *fiber.App) {

	group := app.Group(controller.Config.Route)

	group.Get("/", func(c *fiber.Ctx) error {
		return c.SendString("Hello, welcome to Web Service " + app.Config().AppName + "!")
	})

	group.Post("/login", controller.Authentication)

}

// Authentication func Authenticate user.
// @Description authenticate user.
// @Summary authenticate user
// @Tags Authenticate user
// @Accept json
// @Produce json
// @Param request body criteria.AuthCriteria true "Request Body"
// @Success 200 {object} result.ResponseResult
// @Router /login [post]
func (controller AuthController) Authentication(c *fiber.Ctx) error {
	var authCriteria criteria.AuthCriteria
	err := c.BodyParser(&authCriteria)
	exception.PanicLogging(err)

	userResult, err := controller.AuthRepository.Authentication(c.Context(), authCriteria.Username)
	if err != nil {
		utils.NewLogger().Info(constant.ERR_OBJECT_VALIDATION_DETAIL, ": ", err.Error())
		panic(exception.UnauthorizedError{
			Message: err.Error(),
		})
	}

	passwordDecrypt, errEncrypt := utils.DecryptAes256Sha256([]byte(authCriteria.Password), constant.KEY_PASS_AES)
	if errEncrypt != nil {
		utils.NewLogger().Info(constant.ERR_OBJECT_VALIDATION_DETAIL, ": ", errEncrypt.Error())
		panic(exception.UnauthorizedError{
			Message: errEncrypt.Error(),
		})
	}

	err = bcrypt.CompareHashAndPassword([]byte(userResult.Password), []byte(passwordDecrypt))
	if err != nil {
		utils.NewLogger().Info(constant.ERR_OBJECT_VALIDATION_DETAIL, ": ", "incorrect username and password")
		panic(exception.UnauthorizedError{
			Message: "incorrect username and password",
		})
	}

	tokenJwtResult := utils.GenerateToken(userResult.Username, nil, controller.Config)

	return c.JSON(result.ResponseResult{
		ResponseCode:        http.StatusOK,
		ResponseDescription: constant.SUCCESS,
		ResponseTime:        utils.DateToStdNow(),
		ResponseDatas:       tokenJwtResult,
	})
}
